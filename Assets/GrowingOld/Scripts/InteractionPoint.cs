﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections;
using BlurryRoots.Events;
using BlurryRoots.Common;

public class InteractionPoint : MonoBehaviour {

    public enum Type {
        None,
        ChangeLevel,
        AnimateSun,
        ActivateObject,
    }

    public Type type;

    public TData GetData<TData> () {
        return this.GetComponent<TData> ();
    }

    private void OnChangeLevelTrigger (InteractionPoint ip) {
        var data = ip.GetData<ChangeLevelData> ();
        Assert.IsNotNull (data, "Missing ChangeLevelData!");

        //Application.LoadLevel (data.targetLevel);
        this.eventBus.Raise (data);
    }

    private void OnAnimateSunTrigger (InteractionPoint ip) {
        var data = ip.GetData<AnimateSunData> ();
        Assert.IsNotNull (data, "Missing AnimateSunData!");

        //data.sunController.QueueNextAnimation ();
        this.eventBus.Raise (data);
    }

    internal void OnTriggerEnter (Collider other) {
        Debug.Log ("Collision with " + other.name);

        switch (this.type) {
            case InteractionPoint.Type.AnimateSun:
                this.OnAnimateSunTrigger (this);
                break;
            case InteractionPoint.Type.ChangeLevel:
                this.OnChangeLevelTrigger (this);
                break;
        }
    }

    internal void Start () {
        this.eventBus = ComponentLocator<EventBusSystem>.LocateSingle ().EventBus;
    }

    private EventBus eventBus;

}
