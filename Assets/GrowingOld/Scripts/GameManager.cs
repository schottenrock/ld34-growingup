﻿using UnityEngine;
using BlurryRoots.Common;
using BlurryRoots.Events;
using BlurryRoots.Inputs;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    internal void Awake () {
        this.eventBus = this.GetComponent<EventBusSystem> ().EventBus;
        this.inputManager = this.GetComponent<InputSystem> ().InputManager;
    }

	internal void Start () {
        this.eventBus.Subscribe<ChangeLevelData> (this.OnChangeLevel);

        this.inputManager.RegisterAxis ("Fire1");
        this.inputManager.RegisterAxis ("Fire2");

        MonoBehaviour.DontDestroyOnLoad (this);
    }

    private void OnChangeLevel (ChangeLevelData e) {
        SceneManager.LoadScene (e.targetLevel);
    }

    internal void Update () {
	    if (SceneManager.GetActiveScene().buildIndex == 6 && 0f != Input.GetAxis ("Fire1")) {
            Application.Quit ();
        }
	}

    private EventBus eventBus;
    private InputManager inputManager;

}
