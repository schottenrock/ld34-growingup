﻿using UnityEngine;
using System.Collections.Generic;
using BlurryRoots.Common;
using BlurryRoots.Commands;
using BlurryRoots.Events;
using System;

public class SunController : MonoBehaviour {

    public float duration;
    public Vector3[] Snapshots;

    public void QueueNextAnimation () {
        if ((this.currentSnapshot + 1) >= this.Snapshots.Length) {
            return;
        }

        var start = Quaternion.Euler (this.Snapshots[this.currentSnapshot]);
        var end = Quaternion.Euler (this.Snapshots[this.currentSnapshot + 1]);

        this.commands.Enqueue (new AnimateSunCommand (this.transform, start, end, this.duration));

        ++this.currentSnapshot;
    }

    internal void Update () {
        if (0 < this.commands.Count) {
            if (this.commands.Peek ().Finished) {
                this.commands.Dequeue ();
            }
            else {
                this.commands.Peek ().Execute ();
            }
        }
    }

    internal void Start () {
        var em = ComponentLocator<EventBusSystem>.LocateSingle ().EventBus;
        em.Subscribe<AnimateSunData> (this.OnAnimateSun);
    }

    private void OnAnimateSun (AnimateSunData e) {
        this.QueueNextAnimation ();
    }

    internal void Awake () {
        this.commands = new Queue<IDeferredCommand> ();
    }

    private Queue<IDeferredCommand> commands;
    private int currentSnapshot;

}

public interface IDeferredCommand : ICommand {

    bool Finished { get; }

}

public class AnimateSunCommand : IDeferredCommand {

    public bool Finished {
        get;
        private set;
    }

    public void Execute () {
        this.workingTime += Time.deltaTime;
        var progress = this.workingTime / this.duration;
        
        var rotation = Quaternion.Lerp (this.start, this.end, progress);
        this.transform.rotation = rotation;

        this.Finished = !(this.workingTime < this.duration);
    }

    public AnimateSunCommand (Transform transform, Quaternion startRotation, Quaternion endRotation, float duration) {
        this.transform = transform;
        this.start = startRotation;
        this.end = endRotation;
        this.duration = duration;
        this.workingTime = 0f;

        this.Finished = false;
    }

    private Transform transform;
    private Quaternion start;
    private Quaternion end;
    private float duration;
    private float workingTime;

}