﻿using UnityEngine;
using System.Collections;

public class RotatingController : MonoBehaviour {

    public GameObject model;
    public float roundsPerMinute;
	
	void Start () {
	
	}
		
	void Update () {
        this.model.transform.Rotate (Vector3.up, Time.deltaTime * roundsPerMinute);
	}

}
