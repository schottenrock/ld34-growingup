﻿using UnityEngine;
using UnityEngine.UI;
using BlurryRoots.Events;
using BlurryRoots.Common;
using System.Collections;
using BlurryRoots.Inputs;
using System;
using UnityEngine.SceneManagement;

public class ActorController : MonoBehaviour {

    public Text thoughBubble;

    public string[] thoughs;
    
    private void OnWalk () {
        if (this.isFinalScene) {
            return;
        }

        var movement = Time.deltaTime * new Vector3 (1, 0, 0);
        this.transform.Translate (movement, Space.World);
    }

    private void OnAct () {
        if (this.currentThought < this.thoughs.Length) {
            this.ShowThought ();

            ++this.currentThought;
        }
        else if (this.isFinalScene) {
            this.camerController.ActivateDyingCamera ();
        }
    }

    private void OnVignetteAnimationFinished () {
        SceneManager.LoadScene (6);
    }

    private void ShowThought () {
        this.thoughBubble.enabled = true;
        this.thoughBubble.text = this.thoughs[this.currentThought];
    }

    internal void Awake () {
        this.thoughBubble.enabled = false;
    }

    internal void Start () {
        this.eventBus = ComponentLocator<EventBusSystem>.LocateSingle ().EventBus;

        this.isFinalScene = SceneManager.GetActiveScene ().buildIndex == 5;

        if (this.isFinalScene) {
            this.camerController = FindObjectOfType<CameraController> ();
            this.camerController.VignetteAnimationFinish += this.OnVignetteAnimationFinished;
        }
    }

    internal void Update () {
#if UNITY_EDITOR
        if (Input.GetKeyDown (KeyCode.F1)) {
            SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
        }
#endif
        this.UpdateInput ();
    }

    private void UpdateInput () {
        if (this.isWalking) {
            this.OnWalk ();
        }

        var walk = Input.GetAxis ("Fire1");
        if (0 < walk && !this.isWalking) {
            this.isWalking = true;
        }
        else if (0 == walk && this.isWalking) {
            this.isWalking = false;
        }

        var act = Input.GetAxis ("Fire2");
        if (0 < act && !this.isActing) {
            this.isActing = true;
            this.OnAct ();
        }
        else if (0 == act && this.isActing) {
            this.isActing = false;
        }
    }

    private EventBus eventBus;
    private bool isWalking;
    private bool isActing;
    private int currentThought;
    private bool isFinalScene;
    private CameraController camerController;

}
