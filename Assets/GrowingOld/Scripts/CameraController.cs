﻿using UnityEngine;
using System.Collections;
using BlurryRoots;
using UnityStandardAssets.ImageEffects;

public class CameraController : BlurryBehaviour {

    public System.Action VignetteAnimationFinish;

    public Camera standard;
    public Camera dying;

    public float vignetteAnimationDuration;
    public AnimationCurve animationBehaviour;
    public float pauseBeforeEndDuration;

    protected override void OnStart () {
        this.vignette = this.dying.GetComponent<VignetteAndChromaticAberration> ();
        this.dying.enabled = false;

        this.standard.enabled = true;
    }

    public void ActivateDyingCamera () {
        this.standard.enabled = false;
        this.dying.enabled = true;

        this.StartCoroutine (this.AnimateVignette ());
    }

    private IEnumerator AnimateVignette () {
        var music = FindObjectOfType<GameManager> ().GetComponent<AudioSource> ();
        var startVolume = music.volume;
        var timePassed = 0f;
        var start = this.vignette.intensity;
        var end = 1f;
        var blurStart = this.vignette.blurSpread;
        var blurEnd = 1f;
        while (timePassed < this.vignetteAnimationDuration) {
            var t = timePassed / this.vignetteAnimationDuration;
            var realT = this.animationBehaviour.Evaluate (t);
            this.vignette.intensity = Mathf.Lerp (start, end, realT);
            this.vignette.blurSpread = Mathf.Lerp (blurStart, blurEnd, realT);
            music.volume = Mathf.Lerp (startVolume, 0f, t);

            timePassed += Time.deltaTime;
            yield return null;
        }

        this.vignette.intensity = end;
        this.vignette.blurSpread = blurEnd;

        music.volume = 0;
        music.Stop ();

        yield return new WaitForSeconds (this.pauseBeforeEndDuration);

        if (null != this.VignetteAnimationFinish) {
            this.VignetteAnimationFinish ();
        }
    }

    private VignetteAndChromaticAberration vignette;

}
